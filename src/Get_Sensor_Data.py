from gps import *
import threading
import sqlite3
from time import *
import time
import datetime
import sys
import os
#import Get_Acc_Data

gpsd = None
gpsp = None

class GpsPuller(threading.Thread):

  def __init__(self):
    threading.Thread.__init__(self)
    global gpsd #bring it in scope
    gpsd = gps(mode=WATCH_ENABLE) #starting the stream of info
    self.current_value = None
    self.running = True #setting the thread running to true
 
  def run(self):
    global gpsd
    global gpsp
    while gpsp.running:
      gpsd.next() #this will continue to loop and grab EACH set of gpsd info to clear the buffer


def connect_to_db(database):

    conn = sqlite3.connect(database)
    return conn


def main():

    if __name__ == '__main__':
        global gpsp
        database = connect_to_db('GPS_DATA.db')
       # acc = Get_Acc_Data.get_gyroskop_output()
        os.system("sudo gpsd /dev/ttyUSB0 -F /var/run/gpsd.sock")
        gpsp = GpsPuller()  # create the thread
        try:
            
            gpsp.start()  # start it up
            while True:  # putt code for Database in her

                time.sleep(5)
                current_time = datetime.datetime.now()



                date_string = "{}-{}-{} {}:{}:{}".format(current_time.year,
                                            current_time.month,
                                            current_time.day,
                                            current_time.hour,
                                            current_time.minute,
                                            current_time.second)
                date_string = "CURRENT_TIMESTAMP"
                try:
                    database.execute("INSERT INTO current_gps VALUES (  {} , {} , {} );".format( date_string, 
                                                                        gpsd.fix.longitude, 
                                                                        gpsd.fix.latitude))
                except sqlite3.Error as er:
                    print("error {} , {} , {} , {});".format(   date_string,  
                                                            gpsd.fix.longitude, 
                                                            gpsd.fix.latitude, er))

                    #sys.exit("i don't know :(")  # error handling should be here ;) implement it

                database.commit()
                print("committed")
                #print(acc)
        except (KeyboardInterrupt, SystemExit):
            print("Error")
            gpsp.running = False
            gpsp.join()
            sys.exit()


main()
