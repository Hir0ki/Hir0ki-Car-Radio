import sqlite3
import sys
import os

class datenbase:

    def __init__(self, databaseName ):
        self.databaseName = databaseName
        self.databaseTables = []
        self.connection = self.connectToDatabase()

    def connectToDatabase(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        try:
            conn = sqlite3.connect(dir_path + '/' + self.databaseName)
        except sqlite3.Error as error:
            sys.exit("Database could not connect, with error" + error)
        return conn
    
    def getDatabaseTables(self):
        currentDbTabales = self.connection.execute("SELECT name FROM sqlite_master WHERE type='table';")
        self.databaseTables = []
        for entry in currentDbTabales:
            self.databaseTables.append(entry)
        
    def createTable(self, tableName, tableSchema):
        self.databaseTables.append(databaseTable(self.connection, tableName, tableSchema))

class databaseTable(datenbase):

    def __init__(self, databaseConnection, tableName, tableSchema):
        self.databaseConnection = databaseConnection
        self.tableName = tableName
        self.tableSchema = tableSchema
        self.__createDatabaseTable()

    def __createDatabaseTable(self):
        try:
            self.databaseConnection.execute('''CREATE TABLE IF NOT EXISTS {}
            ( {} );
            '''.format(self.tableName, self.tableSchema))
        except sqlite3.Error as error :
            sys.exit("Error while creating {} Table, with error message: {}".format(
                      self.tableName, error))
        self.databaseConnection.commit()
